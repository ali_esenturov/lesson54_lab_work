package lab_work.lesson_54;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson54Application {
    public static void main(String[] args) {
        System.out.println("hello");
        SpringApplication.run(Lesson54Application.class, args);
    }
}