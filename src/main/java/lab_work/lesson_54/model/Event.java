package lab_work.lesson_54.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "events")
@Data
public class Event {
    @Id
    private String id;

    @Indexed
    private LocalDateTime ldt_event;
    @Indexed
    private String name;
    private String description;

    public Event(String name, String description, LocalDateTime ldt_event) {
        this.name = name;
        this.description = description;
        this.ldt_event = ldt_event;
    }
}
