package lab_work.lesson_54.model;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SubscriptionRepository extends CrudRepository<Subscription,String> {
    Subscription getById(String id);
    List<Subscription> getAllByEmail(String email);
    void deleteById(String id);
}
