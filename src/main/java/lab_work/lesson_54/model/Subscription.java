package lab_work.lesson_54.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document(collection = "subscriptions")
@Data
public class Subscription {
    @Id
    private String id;

    @DBRef
    Event event;
    @Indexed
    private String email;
    private LocalDateTime ldt_sub;

    public Subscription(Event event, String email) {
        this.event = event;
        this.email = email;
        this.ldt_sub = LocalDateTime.now();
    }
}
