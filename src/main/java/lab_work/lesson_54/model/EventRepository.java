package lab_work.lesson_54.model;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EventRepository extends CrudRepository<Event, String> {
    List<Event> getAllBy();
    Event getById(String id);
}
