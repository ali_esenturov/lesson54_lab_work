package lab_work.lesson_54.controller;

import lab_work.lesson_54.services.SubscriptionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subscriptions")
public class SubscriptionController {
    private final SubscriptionService ss;

    public SubscriptionController(SubscriptionService ss){
        this.ss = ss;
    }

    @PostMapping(path = "/subscribe/{event_id}/{email}")
    public String subscribeToEvent(@PathVariable String event_id, @PathVariable String email){
        return ss.subscribe(event_id, email);
    }

    @PostMapping(path = "/{email}")
    public List<String> checkSubscriptions(@PathVariable String email){
        return ss.getEventsWithEmail(email);
    }

    @PostMapping(path = "/unsubscribe/{event_id}/{email}")
    public String unsubscribeFromEvent(@PathVariable String event_id, @PathVariable String email){
        return ss.unsubscribe(event_id, email);
    }
}
