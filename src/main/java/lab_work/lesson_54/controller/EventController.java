package lab_work.lesson_54.controller;


import lab_work.lesson_54.model.Event;
import lab_work.lesson_54.services.EventService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/events")
public class EventController {
    private final EventService eventService;

    public EventController(EventService eventService){
        this.eventService = eventService;
    }

    @GetMapping("/get")
    public List<Event> getEvents() {
        return eventService.getEvents();
    }
}
