package lab_work.lesson_54.services;

import lab_work.lesson_54.model.Event;
import lab_work.lesson_54.model.EventRepository;
import lab_work.lesson_54.model.Subscription;
import lab_work.lesson_54.model.SubscriptionRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SubscriptionService {
    private SubscriptionRepository sr;
    private EventRepository er;


    public SubscriptionService(SubscriptionRepository sr, EventRepository er){
        this.sr = sr;
        this.er = er;
    }


    public void saveSub(Subscription s){
        sr.save(s);
    }

    public Subscription getById(String id){
        return sr.getById(id);
    }

    public List<Subscription> getAllByEmail(String email){
        return sr.getAllByEmail(email);
    }

    public String subscribe(String event_id, String email){
        var event = er.getById(event_id);

        if(event.getLdt_event().isBefore(LocalDateTime.now())){
            return "This event is past.";
        }
        else {
            if(sr.getAllByEmail(email).
                    stream().
                    anyMatch(sub -> sub.getEvent().getId().equals(event_id))){
                return "You have already registered.";
            }
            Subscription s = new Subscription(event, email);
            saveSub(s);
            String subId = getAllByEmail(email).stream().filter(sub -> sub.getEmail().equals(email)).findFirst().get().getId();
            return subId + ". Success";
        }
    }

    public List<String> getEventsWithEmail(String email){
        List<String> events = new ArrayList<>();
        events = getAllByEmail(email).stream().map(s -> s.getEvent().getName()).collect(Collectors.toList());
        return events;
    }

    public String unsubscribe(String event_id, String email){
        boolean exist = sr.getAllByEmail(email).stream().anyMatch(sub -> sub.getEvent().getId().equals(event_id));
        if(exist){
            String sub_id = sr.getAllByEmail(email).stream().filter(sub -> sub.getEvent().getId().equals(event_id)).findFirst().get().getId();
            sr.deleteById(sub_id);
            return "Subscription is successfully deleted.";
        }
        else{
            return "You were not subscribed to this event";
        }
    }
}
