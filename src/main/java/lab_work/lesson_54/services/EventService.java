package lab_work.lesson_54.services;

import lab_work.lesson_54.model.Event;
import lab_work.lesson_54.model.EventRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventService {
    private EventRepository er;

    public EventService(EventRepository er){
        this.er = er;
    }

    public List<Event> getEvents() {
        return er.getAllBy();
    }
    public Event getEventById(String id){return er.getById(id);}
}
