package lab_work.lesson_54.util;


import lab_work.lesson_54.model.Event;
import lab_work.lesson_54.model.EventRepository;
import lab_work.lesson_54.model.Subscription;
import lab_work.lesson_54.model.SubscriptionRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


@Configuration
public class PreloadDatabaseWithData {
    private static Random r = new java.util.Random();

    @Bean
    CommandLineRunner initDatabase(EventRepository eventRepository, SubscriptionRepository subscriptionRepository) {
        return(args) -> {
            eventRepository.deleteAll();
            subscriptionRepository.deleteAll();

            saveData(eventRepository, subscriptionRepository);
        };
    }

    private void saveData(EventRepository eventRepository, SubscriptionRepository subscriptionRepository){
        int eventAmount = r.nextInt(10) + 10;
        int daysAmount;

        List<Event> events = new ArrayList<>();
        List<Subscription> subs = new ArrayList<>();

        for(int i = 0; i < eventAmount; i++){
            daysAmount =  r.nextInt(11) - 5;
            String name = Generator.makeName();
            String description = Generator.makeDescription();
            LocalDateTime ldt_event = LocalDateTime.now().plus(daysAmount, ChronoUnit.DAYS);

            Event e = new Event(name, description, ldt_event);
            events.add(e);
        }
        eventRepository.saveAll(events);
    }
}
